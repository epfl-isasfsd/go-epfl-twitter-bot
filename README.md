# goepflbot

I am the go.epfl.ch bot - I ❤️ tweets that contain links to go.epfl.ch.
Trained by @ponsfrilus.

## Usage

1. Create the `.env` file based on `.env.sample`.  
   Secrets are obtained from [https://developer.twitter.com/en](https://developer.twitter.com/en).
2. `docker-compose up` or `npm start`.
